/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab03;

/**
 *
 * @author Acer
 */
public class Lab03 {
    
    static String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
    
    static boolean checkWin(String[][] table, String currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }
        }
        for (int col = 0; col < 3; col++) {
            if (checkCol(table, currentPlayer, col)) {
                return true;
            }
        }
        if(checkX1(table,currentPlayer)) return true;
        if(checkX2(table,currentPlayer)) return true;
        return false;
    }

 
    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
         return table[row][0].equals(currentPlayer)&& table[row][1].equals(currentPlayer)&& table[row][2].equals(currentPlayer);
    }
    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
         return table[0][col].equals(currentPlayer)&& table[1][col].equals(currentPlayer)&& table[2][col].equals(currentPlayer);
    }
    private static boolean checkX1(String[][] table, String currentPlayer) {
         return table[0][0].equals(currentPlayer)&& table[1][1].equals(currentPlayer)&& table[2][2].equals(currentPlayer);
    }
    private static boolean checkX2(String[][] table, String currentPlayer) {
         return table[0][2].equals(currentPlayer)&& table[1][1].equals(currentPlayer)&& table[2][0].equals(currentPlayer);
    }
    public static boolean checkDraw(String[][] table) {
       return table[0][0] != " " && table[0][1] != " " && table[0][2] != " " &&table[1][0] != " " && table[1][1] != " " && table[1][2] != " " &&table[2][0] != " " && table[2][1] != " " && table[2][2] != "-";
    }
    public static String[][] getTable() {
        return table;
    }
}
